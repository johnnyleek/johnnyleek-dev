import fs from 'fs';
import path from 'path';

export function getProjectData() {
  const dataDir = path.join(process.cwd(), 'data');
  const projectDir = path.join(dataDir, 'projects');
  const files = fs.readdirSync(projectDir);

  return files.map( (file, index) => {
    const filePath = path.join(projectDir, file);
    const contents = fs.readFileSync(filePath, 'utf8');

    // console.log(`ITERATION ${index}, Contents: ${contents}. File: ${file}`)

    const data = JSON.parse(contents);

    if (data["project_link"] === undefined) {
      data["project_link"] = null;
    }

    return {
      id: index,
      title: data["title"],
      languages: data["languages"],
      description: data["description"],
      project_link: data["project_link"],
      source_link: data["source_link"],
      image: data["image"]
    };
  })
}
