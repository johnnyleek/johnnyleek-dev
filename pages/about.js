import Link from 'next/link';

import Layout from '../components/Layout';
import Card from '../components/Card';

import utilStyles from '../styles/util.module.css';
import styles from '../styles/about.module.css';

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faArrowDown, faGlobeAmericas, faGraduationCap, faMusic, faHome } from '@fortawesome/free-solid-svg-icons';

import VisibilitySensor from 'react-visibility-sensor';

export default function About(props) {

    const NextButton = ({ link }) => {
      return (
        <a href={`#${link}`} className={utilStyles.no_decoration}>
          <div className={`${styles.next_button} ${utilStyles.shadow} animate__animated animate__pulse animate__infinite ${utilStyles.anim_3s}`}>
            <FontAwesomeIcon size={'2x'} icon={faArrowDown}/>
          </div>
        </a>
      );
    }

    const HomeButton = () => {
      return (
        <Link href="/">
          <a className={utilStyles.no_decoration}>
            <div className={`${styles.next_button} ${utilStyles.shadow} animate__animated animate__pulse animate__infinite ${utilStyles.anim_3s}`}>
              <FontAwesomeIcon size={'2x'} icon={faHome}/>
            </div>
          </a>
        </Link>
      );
    }

    return (
      <Layout backButton>
        <div className={`${utilStyles.hero} ${utilStyles.column} ${utilStyles.light_text} ${utilStyles.justify_center}`}>
          <a href="#fun_facts" className={utilStyles.no_decoration}>
            <div className={`${styles.header_circle} ${utilStyles.circle} ${utilStyles.shadow} animate__animated animate__pulse animate__infinite ${utilStyles.anim_3s}`}>
              <h1>About Me</h1>
              <FontAwesomeIcon size={'2x'} icon={faArrowDown}/>
            </div>
          </a>
        </div>
        <div id="fun_facts" className={`${utilStyles.hero} ${utilStyles.column} ${utilStyles.light_text} ${utilStyles.justify_center}`}>
          <VisibilitySensor partialVisibility>
          {({ isVisible }) => {
            return (
            <div className={`${styles.about_section_container} ${isVisible ? utilStyles.no_opacity : utilStyles.transparent}`}>
              <h1>Fast Facts</h1>
              <div className={styles.fast_fact_container}>
                <Card noLink
                  id="age_card"
                  header="Hometown"
                  icon={faGlobeAmericas}
                  description="I'm originally from England, but I now live in Michigan, USA!"
                />
                <Card noLink noIcon
                  id="programming_card"
                  header="I was"
                  noIconText="9"
                  description="Years old when I wrote my first program (Hello World in C++)!"
                />
                <Card noLink
                  id="college_card"
                  header="College"
                  icon={faGraduationCap}
                  description="I'm currently attending Central Michigan University (Class of 2023)"
                />
                <Card noLink
                  id="music_card"
                  header="Music"
                  icon={faMusic}
                  description="I play the trombone and have an instagram account dedicated to videos of me playing it!"
                />
              </div>
              <NextButton link="hometown"/>
            </div>
            );
          }}
          </VisibilitySensor>
        </div>
        <div id="hometown" className={`${utilStyles.hero} ${utilStyles.column} ${utilStyles.light_text} ${utilStyles.justify_center}`}>
          <VisibilitySensor partialVisibility>
          {({ isVisible }) => {
            return (
            <div className={`${styles.about_section_container} ${isVisible ? utilStyles.no_opacity : utilStyles.transparent}`}>
              <h1>Hometown</h1>
              <div>
                <p>
                  I was born in Nottingham, England, where I lived for 9 years! In 2009,
                  my father was offered a job out in America, and he took it!
                </p>
                <p>
                  On September 22, 2010, we made the big move and I have lived here
                  ever since!
                </p>
                <p>
                  All of my family still lives in England, and I've only been back to
                  visit once. However, I'm still not a US citizen (I am a permanent resident,
                  though).
                </p>
                <p>
                  I'm scheduled to become a citizen in September of 2020. You're not
                  eligible to apply until you've lived here for at least 10 years.
                </p>
                <p>
                  I am so thankful for the opportunities that I have been given. Being
                  able to experience living in two separate countries is a wonderful blessing.
                </p>
                <p>
                  I'm a very open-minded individual always willing to try new experiences,
                  and that makes me even more thankful for the life that I have been given.
                </p>
              </div>
              <NextButton link="programming"/>
            </div>
            );
          }}
          </VisibilitySensor>
        </div>
        <div id="programming" className={`${utilStyles.hero} ${utilStyles.column} ${utilStyles.light_text} ${utilStyles.justify_center}`}>
          <VisibilitySensor partialVisibility>
          {({ isVisible }) => {
            return (
            <div className={`${styles.about_section_container} ${isVisible ? utilStyles.no_opacity : utilStyles.transparent}`}>
              <h1>Programming</h1>
              <div>
                <p>
                  As aforementioned in the cards above, I wrote my first computer program
                  when I was 9 years old.
                </p>
                <p>
                  It was just after I moved to America. My dad taught me how to write
                  both "Hello World!" and a calculator in C++.
                </p>
                <p>
                  Since that day, I have had a burning passion for computers and programming.
                  I'm always creating new things, and my desire has only grown as I've become
                  older.
                </p>
                <p>
                  I'm a logical thinker, and a problem solver. I love piecing together
                  parts of a larger puzzle in order to create results.
                </p>
                <p>
                  I think ahead to create scalable and reliable systems that save my team
                  time and effort in the long term.
                </p>
                <p>
                  Programming is by far one of my biggest hobbies. Usually after a long
                  school day, I'll go back to my room and start programming to relieve
                  my stress.
                </p>
                <p>
                  I would certainly describe myself as an aspiring software engineer. It's
                  been a goal of mine since I started programming to do it as a career.
                </p>
                <p>
                  I dream of working in a progressive workplace using cutting edge
                  technology to make the world a better place with a diverse team of
                  all backgrounds.
                </p>
              </div>
              <NextButton link="college"/>
            </div>
            );
            }}
          </VisibilitySensor>
        </div>
        <div id="college" className={`${utilStyles.hero} ${utilStyles.column} ${utilStyles.light_text} ${utilStyles.justify_center}`}>
          <VisibilitySensor partialVisibility>
          {({ isVisible }) => {
            return (
            <div className={`${styles.about_section_container} ${isVisible ? utilStyles.no_opacity : utilStyles.transparent}`}>
              <h1>College / Education</h1>
              <div>
                <p>
                  I currently attend Central Michigan University, class of 2023.
                </p>
                <p>
                  I'm earning a bachelors degree in Computer Science, with minors
                  in Multimedia Design and Information Technology.
                </p>
                <p>
                  At the time of updating (June 2020), I have taken quite a few courses
                  relevant to Computer Science. These are:
                </p>
                <ul>
                  <li>Intro to Data Structures</li>
                  <li>Object Oriented Analysis & Design</li>
                  <li>Computer Organization & Communications</li>
                  <li>Intro to Multimedia Design (2D Game Design/Development)</li>
                  <li>Advanced Multimedia Design (3D Game Design/Development)</li>
                </ul>
                <p>
                  I'm also going through my general education program at the same time
                  as these classes.
                </p>
                <p>
                  I've been taking an average of ~20 credits a semester, and work 20 hours
                  a week as well, so I certainly stay busy!
                </p>
                <p>
                  I'm also a member of the Chippewa Marching Band. I've met so many
                  great people through that program and it's one of my favourite on
                  campus organizations.
                </p>
                <p>
                  I was also given an opportunity to be a web developer for the school,
                  so that is where I spend a lot of my time outside of class as well.
                  (More info below)
                </p>
              </div>
              <NextButton link="work"/>
            </div>
            );
            }}
          </VisibilitySensor>
        </div>
        <div id="work" className={`${utilStyles.hero} ${utilStyles.column} ${utilStyles.light_text} ${utilStyles.justify_center}`}>
          <VisibilitySensor partialVisibility>
          {({ isVisible }) => {
            return (
            <div className={`${styles.about_section_container} ${isVisible ? utilStyles.no_opacity : utilStyles.transparent}`}>
              <h1>Work Experience</h1>
              <div>
                <p>
                  I'm currently a Senior Web Developer / Student Manager for CMU
                  Technology Operations (TechOps).
                </p>
                <p>
                  Our team creates and maintains web applications for internal offices
                  around the university. We also create registration sites for certain
                  CMU events.
                </p>
                <p>
                  As it is a student job, one of the things we aim to get out of it
                  is knowledge of technologies as well as work experience.
                </p>
                <p>
                  The technologies we use are:
                </p>
                <ul>
                  <li>General Frontend Languages (HTML, CSS, JS)</li>
                  <li>Django (Python)</li>
                  <li>React.JS</li>
                  <li>PostgreSQL</li>
                  <li>Git/GitLab</li>
                </ul>
                <p>
                  We also aim to use the agile workflow during our day-to-day activities.
                </p>
                <p>
                  One of my main responsibilities as a student manager is to review
                  other developers code and mentor them to grow as developers.
                </p>
                <p>
                  I use <a href="https://google.github.io/eng-practices/review/reviewer/standard.html" rel="noopener noreferrer" target="_blank">Google's Code Review</a> guidelines
                  when going over code and always encourage others to do the same.
                </p>
                <p>
                  I don't just review code however. I also write my own code to contribute
                  to our projects, ensuring I meet our style guidelines and requirements
                  as I go.
                </p>
                <p>
                  I also have taken on a DevOps role as a manager, in that I (along with
                  another manager) am in charge of deploying our websites to our production
                  servers.
                </p>
                <p>
                  That role has me learning new technologies in order to maintain our
                  servers, including Nginx and Fabric.
                </p>
              </div>
              <NextButton link="music"/>
            </div>
            );
          }}
          </VisibilitySensor>
        </div>
        <div id="music" className={`${utilStyles.hero} ${utilStyles.column} ${utilStyles.light_text} ${utilStyles.justify_center}`}>
          <VisibilitySensor partialVisibility>
          {({ isVisible }) => {
            return (
              <div className={`${styles.about_section_container} ${isVisible ? utilStyles.no_opacity : utilStyles.transparent}`}>
                <h1>Music</h1>
                <div>
                  <p>
                    I've been playing the trombone since 6th grade and absolutely love
                    it.
                  </p>
                  <p>
                    I get involved in music wherever my schedule allows. I did marching
                    band throughout highschool and am currently doing it in college.
                  </p>
                  <p>
                    I play lead trombone as part of one of the university jazz bands.
                  </p>
                  <p>
                    I <i>was</i> also part of the trombone choir (until COVID-19 happened
                    and school became online)
                  </p>
                  <p>
                    I have an instagram account dedicated to my trombone playing!
                    If you want to check it out: <a href="https://www.instagram.com/johnnyleektrombone/" rel="noopener noreferrer" target="_blank">@johnnyleektrombone</a>
                  </p>
                </div>
                <NextButton link="end"/>
              </div>
            );
          }}
          </VisibilitySensor>
        </div>
        <div id="end" className={`${utilStyles.hero} ${utilStyles.column} ${utilStyles.light_text} ${utilStyles.text_center} ${utilStyles.justify_center}`}>
          <VisibilitySensor partialVisibility>
          {({ isVisible }) => {
            return (
              <div className={`${styles.about_section_container} ${isVisible ? utilStyles.no_opacity : utilStyles.transparent}`}>
                  <h1>Well, that's all you need to know about me!</h1>
                  <h1>I guess that means we're basically best friends now.</h1>
                  <HomeButton/>
                </div>
            );
          }}
          </VisibilitySensor>
        </div>
      </Layout>
    )
}
