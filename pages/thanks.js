import Layout from '../components/Layout';

import styles from '../styles/thanks.module.css';
import utilStyles from '../styles/util.module.css';

export default function Thanks(props) {
  return (
    <Layout backButton>
      <div className={`${styles.container} ${utilStyles.hero} ${utilStyles.column} ${utilStyles.light_text} ${utilStyles.padding}`}>
        <h1>Thanks!</h1>

        <svg className={`${styles.checkmark}`} xmlns="http://www.w3.org/2000/svg" viewBox="0 0 52 52">
          <circle className={styles.circle} cx="26" cy="26" r="25" fill="#7ac142"/>
          <path className={styles.check} fill="none" d="M14.1 27.2l7.1 7.2 16.7-16.8"/>
        </svg>

        <h3>Your message has been received! I'll get back to you as soon as I can.</h3>
      </div>
    </Layout>
  )
}
