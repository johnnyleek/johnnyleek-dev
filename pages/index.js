import Layout from '../components/Layout';
import Card from '../components/Card';
import styles from '../styles/home.module.css';
import utilStyles from '../styles/util.module.css';
import { faSmile, faRocket, faAddressCard, faEnvelopeOpenText } from '@fortawesome/free-solid-svg-icons';

export default function Main(props) {

  const getAge = () => {
    let ageInMs = Date.now() - new Date(2001, 8, 8);
    let ageAsDate = new Date(ageInMs);

    return Math.abs(ageAsDate.getUTCFullYear() - 1970);
  }

  return (
    <Layout>
      <div className={`${utilStyles.hero} ${utilStyles.column} ${utilStyles.light_text} ${utilStyles.justify_center}`}>
        <img src="/images/profile_pic.png" alt="Profile Picture" className={`${styles.profile_pic} ${utilStyles.circle} ${utilStyles.shadow} ${utilStyles.border_light}`} />
        <h1 className={styles.header}>👋 Hi! I'm Johnny</h1>
        <div className={styles.subheader}>
          <h3>
            I'm a {getAge()} year old aspiring software engineer and web developer
            currently attending Central Michigan University (🔥⬆️ )!
          </h3>
          <h3>
            I made this site to serve as my digital portfolio.
            Any serious project I've made will be on this site.
            Feel free to take a look around!
          </h3>
        </div>
        <div className={styles.card_container}>
          <Card
            id="about_me_card"
            header="About"
            icon={faSmile}
            description="Learn about who I am!"
            href="/about"
          />
          <Card
            id="projects_card"
            header="Projects"
            icon={faRocket}
            description="View my projects!"
            href="/projects"
          />
          <Card
            id="reusme_card"
            header="Resume"
            icon={faAddressCard}
            description="Take a look at my resume!"
            href="/api/view_resume"
          />
          <Card
            id="contact_card"
            header="Contact"
            icon={faEnvelopeOpenText}
            description="Contact me!"
            href="/contact"
          />
        </div>
      </div>
    </Layout>
  );
}
