import { useState, useEffect } from 'react';
import Layout from '../components/Layout';
import ProjectCard from '../components/ProjectCard';

import utilStyles from '../styles/util.module.css';
import styles from '../styles/projects.module.css';
import checkboxStyles from '../styles/checkbox.module.css';

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import {
  faReact,
  faJsSquare,
  faJava,
  faHtml5,
  faCss3Alt,
  faPython
} from '@fortawesome/free-brands-svg-icons';

import { server } from '../utils/server';
import { getProjectData } from '../lib/projects';

export default function Projects(props) {

  const [projects, setProjects] = useState(props.projects);
  const [checkedBoxes, setCheckedBoxes] = useState([]);

  useEffect(() => {
    if(checkedBoxes.length === 0) {
      //If no boxes are selected
      setProjects(props.projects);
    } else {
      let currentProjects = props.projects;
      currentProjects = currentProjects.filter(project => {
        for(let i = 0; i < checkedBoxes.length; i++) {

          for(let j = 0; j < project.languages.length; j++) {
            if (project.languages[j].toLowerCase() === checkedBoxes[i].toLowerCase()) {
              return true;
            }
          }

        }
      });
      setProjects(currentProjects)
    }
  }, [checkedBoxes])

  const onCheck = box => {
    let boxes = [...checkedBoxes];
    if (document.getElementById(box).checked) {
      //Add box
      boxes.push(box);
    } else {
      //Remove box
      let index = 0;
      for (let i = 0; i < boxes.length; i++) {
        if (boxes[i] === box)
          index = i;
      }
      boxes.splice(index, 1)
    }
    setCheckedBoxes(boxes);
  }

  const isChecked = box => {
    return checkedBoxes.includes(box)
  }

  const IconCheckBox = ({ htmlFor, icon, label, checkFunc, checked }) => {
    return (
      <>
      <input type="checkbox" name={htmlFor} id={htmlFor} className={checkboxStyles.checkbox} onChange={() => checkFunc(htmlFor)} checked={checked(htmlFor)}/>
      <div className={`${checkboxStyles.container} ${utilStyles.shadow}`}>
        <label htmlFor={htmlFor} className={checkboxStyles.checkbox_label}><FontAwesomeIcon icon={icon} size={'3x'}/></label>
        <label htmlFor={htmlFor} className={checkboxStyles.checkbox_label}>{label}</label>
      </div>
      </>
    );
  }

  return (
    <Layout backButton>
      <div className={`${utilStyles.hero} ${utilStyles.column} ${utilStyles.light_text} ${utilStyles.padding}`}>

        <div id={styles.language_selector}>
          <IconCheckBox htmlFor="react" icon={faReact} label="React.js" checkFunc={onCheck} checked={isChecked}/>
          <IconCheckBox htmlFor="html" icon={faHtml5} label="HTML" checkFunc={onCheck} checked={isChecked}/>
          <IconCheckBox htmlFor="css" icon={faCss3Alt} label="CSS" checkFunc={onCheck} checked={isChecked}/>
          <IconCheckBox htmlFor="js" icon={faJsSquare} label="JavaScript" checkFunc={onCheck} checked={isChecked}/>
          <IconCheckBox htmlFor="python" icon={faPython} label="Python" checkFunc={onCheck} checked={isChecked}/>
          <IconCheckBox htmlFor="java" icon={faJava} label="Java" checkFunc={onCheck} checked={isChecked}/>
        </div>
        <div id={styles.project_container}>
          {
            projects.map(project => {
              return <ProjectCard
                        key={project.id}
                        title={project.title}
                        languages={project.languages}
                        description={project.description}
                        project_link={project.project_link}
                        source_link={project.source_link}
                        img_src={`/images/${project.image}`}
                     />
            })
          }

        </div>
      </div>
    </Layout>
  )
}

export async function getStaticProps() {
  const projects = getProjectData()

  return {
    props: {
      projects
    }
  }
}
