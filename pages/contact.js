import Layout from '../components/Layout';

import utilStyles from '../styles/util.module.css';
import styles from '../styles/contact.module.css';

export default function Contact(props) {

  return (
    <Layout backButton>
      <div className={`${utilStyles.hero} ${utilStyles.column} ${utilStyles.light_text} ${utilStyles.padding}`}>
        <div className={`${styles.form_container}`}>
          <form action="/api/contact" id="contact-form" method="post" role="form" className={`${styles.form} ${utilStyles.shadow}`}>
            <h1 className={styles.form_header}>Contact Me!</h1>
            <div className={styles.form_element}>
              <label htmlFor="name">Name:</label>
              <input id="name" name="name" type="text" placeholder="John Doe" required/>
            </div>
            <div className={styles.form_element}>
              <label htmlFor="email">Email:</label>
              <input id="email" name="email" type="text" placeholder="john@gmail.com" required/>
            </div>
            <div className={styles.form_element}>
              <label htmlFor="message">Message:</label>
              <textarea id="message" name="message" placeholder="I just messaged to say hi!" rows="3" required></textarea>
            </div>
            <button type="submit" className={utilStyles.shadow}>Submit</button>
          </form>
        </div>
      </div>
    </Layout>
  )

}
