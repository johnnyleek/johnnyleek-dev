import { useEffect } from 'react';

import { getTheme, setTheme } from '../utils/theme';
import '../styles/global.css';

export default function App({ Component, pageProps }) {
  useEffect(() => {
      if(getTheme() === 'dark_theme') setTheme('dark_theme');
      else setTheme('light_theme');
  }, []);

  return <Component {...pageProps} />
}
