const sgMail = require('@sendgrid/mail')

export default async (req, res) => {
    sgMail.setApiKey(process.env.SENDGRID_API_KEY);
    const { name, message, email } = req.body;

    const msg = {
        to: 'leek1j@cmich.edu',
        from: 'johnnyleek2001@gmail.com' ,
        subject: 'New Message',
        text: `${name} sent you a message!\n\nIt says:\n${message}\n\nContact Info:\n${email}`
    }

    try {
        await sgMail.send(msg)
        res.status(200).writeHead(302, { 'Location': '/thanks' }).end();
    } catch (error) {
        console.error(`ERROR: ${error}`);
        console.error(`MESSAGE: ${error.message}`);
        res.status(400).writeHead(302, { 'Location': '/send_error' }).end()
    }
}
