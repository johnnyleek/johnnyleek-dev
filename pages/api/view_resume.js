import fs from 'fs';
import path from 'path';

export default (req, res) => {
    const publicDir = path.join(process.cwd(), 'public');

    const filePath = path.join(publicDir, 'Johnny Leek Resume.pdf');
    const data = fs.readFileSync(filePath);

    res.status(200).writeHead(200, { 'Content-Type': 'application/pdf' });
    res.end(data, 'binary');
}
