import { getProjectData } from '../../lib/projects';

export default (req, res) => {
  res.statusCode = 200;
  res.json(getProjectData());
}
