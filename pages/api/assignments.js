import fs from 'fs';
import path from 'path';

export default (req, res) => {
    const publicDir = path.join(process.cwd(), 'public');
    const assignmentDir = path.join(publicDir, 'assignments');

    const filePath = path.join(assignmentDir, 'a3.pdf');
    const data = fs.readFileSync(filePath);

    res.status(200).writeHead(200, { 'Content-Type': 'application/pdf' });
    res.end(data, 'binary');
}
