import Layout from '../components/Layout';

import styles from '../styles/thanks.module.css';
import utilStyles from '../styles/util.module.css';

export default function Maintenance(props) {
    return (
        <Layout backButton>
          <div className={`${styles.container} ${utilStyles.hero} ${utilStyles.column} ${utilStyles.light_text} ${utilStyles.padding}`}>
            <h1>Sorry!</h1>
            <h3>The contact form is currently under maintenance.</h3>
            <br/><br/>
            <h3>I'm working on this, and will have it fixed as soon as I can!</h3>
        </div>
    </Layout>

    );
}
