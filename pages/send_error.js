import Layout from '../components/Layout';

import styles from '../styles/thanks.module.css';
import utilStyles from '../styles/util.module.css';

export default function SendError(props) {
    return (
        <Layout backButton>
          <div className={`${styles.container} ${utilStyles.hero} ${utilStyles.column} ${utilStyles.light_text} ${utilStyles.padding}`}>
            <h1>Sorry!</h1>
            <h2>Something went wrong.</h2>
            <br/>
            <h3>Rest assured, I'm working on this and will have it fixed as soon as I can!</h3>
        </div>
    </Layout>

    );
}
