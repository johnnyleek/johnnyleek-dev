import {
  faReact,
  faJsSquare,
  faJava,
  faHtml5,
  faCss3Alt,
  faPython
} from '@fortawesome/free-brands-svg-icons';

export const getIconFromLanguage = language => {
  let languages = {
    "react": faReact,
    "js": faJsSquare,
    "java": faJava,
    "html": faHtml5,
    "css": faCss3Alt,
    "python": faPython
  }
  return languages[language.toLowerCase()]
}
