export const setTheme = theme => {
  localStorage.setItem('theme', theme);
  document.documentElement.className = theme;
}

export const getTheme = () => {
  return localStorage.getItem('theme');
}
