const isDevelopent = process.env.NODE_ENV !== 'production';

export const server = isDevelopent ? 'http://localhost:3000' : 'https://johnnyleek.dev'
