import { useState, useEffect } from 'react';

import styles from '../styles/backtotop.module.css';
import utilStyles from '../styles/util.module.css';

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faChevronUp } from '@fortawesome/free-solid-svg-icons';

export default function BackToTop() {
  return (
    <a href="#">
      <div id={styles.back_to_top} className={`${utilStyles.light_text} ${utilStyles.shadow}`}>
        <div className={utilStyles.padding}>
          <FontAwesomeIcon size={'1x'} icon={faChevronUp} />
          <span className={styles.back_text}>
            Back to Top
          </span>
        </div>
      </div>
    </a>
  )
}
