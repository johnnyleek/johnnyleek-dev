import Link from 'next/link';

import styles from '../styles/card.module.css';
import utilStyles from '../styles/util.module.css';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faSmile } from '@fortawesome/free-solid-svg-icons';

export default function Card(props) {
  const CardContent = () => {
    return (
      <div id={props.id} className={`${styles.card} ${utilStyles.shadow} ${props.className}`}>
        <h1 className={styles.card_header}>{props.header}</h1>
        <div className={styles.card_contents}>
          {props.noIcon
            ? <h1 className={styles.no_icon_text}>{props.noIconText}</h1>
            : <h1><FontAwesomeIcon size={'3x'} icon={props.icon}/></h1>
          }
          <p className={styles.card_text}>
            {props.description}
          </p>
        </div>
      </div>
    );
  }

  return (
    <>
    {props.noLink
      ? <CardContent />
      :
        <Link href={`${props.href}`}>
          <a className={utilStyles.no_decoration}>
            <CardContent/>
          </a>
        </Link>
    }
    </>
  );
}

Card.defaultProps = {
  id: "card",
  href: "/",
  header: "Card",
  icon: faSmile,
  description: "This is a card.",
  className: ""
}
