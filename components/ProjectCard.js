import styles from '../styles/projectcard.module.css';
import utilStyles from '../styles/util.module.css';

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { getIconFromLanguage } from '../utils/iconfromlanguage';

export default function ProjectCard(props) {

  /*return (
    <div className={`${styles.project_card} ${utilStyles.shadow}`}>
      <div className={`${styles.header}`}>
        <h1>{props.title}</h1>
      </div>
      <div className={`${styles.content}`}>
        <img className={`${styles.content_img}`}
             src={props.img_src}
        />
        <div className={`${styles.grey_bg}`}>
        <div className={styles.description}>
          <p>{props.description}</p>
        </div>
        </div>
      </div>
      <div className={`${styles.footer}`}>
        {props.languages.map(language => {
          return <FontAwesomeIcon icon={getIconFromLanguage(language)} size={'2x'}/>
        })}
      </div>
    </div>
  );*/

  return (
    <div className={`${styles.card} ${utilStyles.shadow}`}>
      <div className={`${styles.card_header}`}>
        <img src={props.img_src} alt={props.title}/>
        <div className={`${styles.grey_bg}`}>
          {props.project_link !== undefined
            ?
              <>
              <a href={props.project_link} target="_blank" rel="noopener noreferrer">
                Project Link
              </a>
              <br/>
              </>
            : null}

          <a href={props.source_link} target="_blank" rel="noopener noreferrer">
            Source Code Link
          </a>
        </div>
      </div>

      <div className={`${styles.card_body}`}>
        <div className={`${styles.card_content}`}>
          <h1>{props.title}</h1>
          <p>
            {props.description}
          </p>
        </div>
        <div className={`${styles.card_footer}`}>
          {props.languages.map(language => {
            return <FontAwesomeIcon icon={getIconFromLanguage(language)} size={'2x'}/>
          })}
        </div>
      </div>
    </div>
  )

}
