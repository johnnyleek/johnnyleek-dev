import { useState, useEffect } from 'react';

import { getTheme, setTheme } from '../utils/theme.js';
import styles from '../styles/themeswitch.module.css';

export default function ThemeSwitch() {
  const [checked, setChecked] = useState(false);

  useEffect(() => {
    if(getTheme() === 'dark_theme') setChecked(true);
  }, []);

  const handleCheck = e => {
    let theme = getTheme() === 'dark_theme' ? 'light_theme' : 'dark_theme';
    setTheme(theme);
    setChecked(!checked);
  }

  return (
    <div id={styles.theme_toggler}>
      <input type="checkbox" id={styles.theme_input} checked={checked} onChange={handleCheck}/>
      <label htmlFor={styles.theme_input} id={styles.theme_label}>
        <span id={styles.moon_craters}>
          <span className={`${styles.crater} ${styles.crater1}`}/>
          <span className={`${styles.crater} ${styles.crater2}`}/>
          <span className={`${styles.crater} ${styles.crater3}`}/>
        </span>
        <span className={`${styles.star} ${styles.star1}`}/>
        <span className={`${styles.star} ${styles.star2}`}/>
        <span className={`${styles.star} ${styles.star3}`}/>
        <span className={`${styles.star} ${styles.star4}`}/>
        <span className={`${styles.star} ${styles.star5}`}/>
        <span className={`${styles.star} ${styles.star6}`}/>
      </label>
    </div>
  );
}
