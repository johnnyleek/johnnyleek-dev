import { useState } from 'react';
import Head from 'next/head';

import ThemeSwitch from './ThemeSwitch';
import BackToHome from './BackToHome';
import BackToTop from './BackToTop';

export default function Layout({ children, backButton }) {
  const [scroll, setScroll] = useState(0);

  //Only add event if run on client
  if(process.browser) {
    window.addEventListener('scroll', e => {
      setScroll(window.scrollY);
    });
  }

  return (
    <div>
      <Head>
        <link rel="icon" href="/favicon.ico"/>
        <meta name="viewport" content="initial-scale=1.0, width=device-width"/>
        <meta
          name="description"
          content="Johnny Leek's Development Portfolio"
        />
        <title>Johnny Leek | Developer & Student</title>

        <script src="https://kit.fontawesome.com/0ce612fbe8.js" crossOrigin="anonymous"></script>
        <link
          rel="stylesheet"
          href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/4.0.0/animate.min.css"
        />

      </Head>
      {backButton ? <BackToHome/> : null}
      <ThemeSwitch/>
      {children}
      {scroll > 200 ? <BackToTop/> : null}
    </div>
  )
}
