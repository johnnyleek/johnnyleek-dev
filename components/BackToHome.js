import styles from '../styles/backtohome.module.css';
import utilStyles from '../styles/util.module.css';

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faBackward } from '@fortawesome/free-solid-svg-icons';

import Link from 'next/link';

export default function BackToHome() {
  return (
      <Link href="/">
        <a>
          <div id={styles.back_to_home} className={`${utilStyles.light_text} ${utilStyles.shadow}`}>
            <div className={utilStyles.padding}>
              <FontAwesomeIcon size={'1x'} icon={faBackward} />
              <span className={styles.back_text}>
                Go Back
              </span>
            </div>
          </div>
        </a>
      </Link>
  );
}
